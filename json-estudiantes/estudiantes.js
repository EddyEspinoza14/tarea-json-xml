const bd =[
    {"Id":0,"Apellido":"Belen", "Nombre":"Soriano", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0987654321",
    "Correo":"belen@gmail.com"},
    {"Id":1,"Apellido":"Monteagudo", "Nombre":"Jorge", "Semestre": "Quinto",   
    "Paralelo":"B", "Direccion":"Montecristi", "Telefono": "0987654322",
    "Correo":"jorge@gmail.com"},
    {"Id":2,"Apellido":"Sala", "Nombre":"Vanesa", "Semestre": "Quinto", 
    "Paralelo":"A", "Direccion":"Chone", "Telefono": "0987654323",
    "Correo":"vanessa@gmail.com"},
    {"Id":3,"Apellido":"Carrion", "Nombre":"Marina", "Semestre": "Quinto",   
    "Paralelo":"B", "Direccion":"Canoa", "Telefono": "0987654324",
    "Correo":"marina@gmail.com"},
    {"Id":4,"Apellido":"Mendoza", "Nombre":"juana", "Semestre": "Quinto", 
    "Paralelo":"C", "Direccion":"Portoviejo", "Telefono": "0997654321",
    "Correo":"juana@gmail.com"},
    {"Id":5,"Apellido":"Damian", "Nombre":"Diaz", "Semestre": "Sexto", 
    "Paralelo":"A", "Direccion":"Los Bajos", "Telefono": "0987654311",
    "Correo":"diaz@gmail.com"},
    {"Id":6,"Apellido":"Alvez", "Nombre":"Jonathan", "Semestre": "Sexto", 
    "Paralelo":"B", "Direccion":"Montecristi", "Telefono": "0985654321",
    "Correo":"jonathan@gmail.com"},
    {"Id":7,"Apellido":"Marquez", "Nombre":"Gabriel", "Semestre": "Tercero", 
    "Paralelo":"A", "Direccion":"Manta", "Telefono": "0987644321",
    "Correo":"gabriel@gmail.com"},
    {"Id":8,"Apellido":"Vinces", "Nombre":"Juan", "Semestre": "Cuarto", 
    "Paralelo":"A", "Direccion":"Portoviejo", "Telefono": "0977654321",
    "Correo":"juan@gmail.com"},
    {"Id":9,"Apellido":"Bailon", "Nombre":"Aimar", "Semestre": "Cuarto", 
    "Paralelo":"B", "Direccion":"Guayaquil", "Telefono": "0987655321",
    "Correo":"aimar@gmail.com"}
]

const estudiantes = document.querySelectorAll('.nombre-estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista">

                                        <div class="nom">
                                        <h2>Nombre:</h2>
                                        <p>${estudiante.Nombre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Apellido:</h2>
                                            <p>${estudiante.Apellido}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Correo:</h2>
                                            <p>${estudiante.Correo}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Telefono:</h2>
                                            <p>${estudiante.Telefono}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Direccion:</h2>
                                            <p>${estudiante.Direccion}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Semestre:</h2>
                                            <p>${estudiante.Semestre}</p>
                                        </div>
                                        <div class="nom">
                                            <h2>Paralelo:</h2>
                                            <p>${estudiante.Paralelo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})